import { browser, logging, element, by } from 'protractor';

describe('angularjs homepage', function() {

  beforeEach(function() {
    browser.get('http://www.way2automation.com/angularjs-protractor/registeration/#/login');
  });


  it('should greet the named user', function() {
    element(by.model('yourName')).sendKeys('Julie');
    var greeting = element(by.binding('yourName'));
    expect(greeting.getText()).toEqual('Hello Julie!');
  });
});
